<?php
// Heading
$_['heading_title']     = 'Danh mục Mô-đun đã cài';

// Text
$_['text_success']      = 'Chúc mừng! Bạn đã cập nhật thành công!';
$_['text_refresh']      = 'Sau khi bạn Kích hoạt / Tắt hay xóa 1 Mô-đun chức năng mở rộng, bạn cần phải Click nút Cập Nhật để làm mới bộ nhớ Cache!';
$_['text_list']         = 'Danh mục Mô-đun';

// Column
$_['column_name']       = 'Tên Mô-đun';
$_['column_author']     = 'Tác giả';
$_['column_version']    = 'Phiên bản';
$_['column_status']     = 'Trạng thái';
$_['column_date_added'] = 'Ngày thêm';
$_['column_action']     = 'Thao tác';

// Error
$_['error_permission']  = 'Cảnh báo! Bạn không có quyền chỉnh sửa!';