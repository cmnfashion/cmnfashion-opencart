<?php
// Heading
$_['heading_title']     = 'Quản lý bộ Lọc';

// Text
$_['text_success']      = 'Chúc mừng! Bạn đã cập nhật thành công!';
$_['text_list']         = 'Danh sách nhóm bộ lọc';
$_['text_add']          = 'Thêm bộ lọc';
$_['text_edit']         = 'Cập nhật bộ lọc';

// Column
$_['column_group']      = 'Nhóm bộ lọc';
$_['column_sort_order'] = 'Sắp xếp';
$_['column_action']     = 'Thao tác';

// Entry
$_['entry_group']       = 'Tên nhóm bộ lọc';
$_['entry_name']        = 'bộ lọc';
$_['entry_sort_order']  = 'Sắp xếp';

// Error
$_['error_permission']  = 'Cảnh báo! Bạn không có quyền chỉnh sửa bộ lọc!';
$_['error_group']       = 'Tên nhóm bộ lọc phải có từ 3 đến 64 ký tự!';
$_['error_name']        = 'Tên bộ lọc phải có từ 3 đến 64 ký tự!';