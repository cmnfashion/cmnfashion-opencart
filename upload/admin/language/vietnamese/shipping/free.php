<?php
// Heading
$_['heading_title']    = '<font color="green"><b>Miễn phí Giao hàng</b></font>';

// Text 
$_['text_shipping']    = 'Shipping';
$_['text_success']     = 'Thành Công: Bạn đã thay đổi free shipping!';
$_['text_edit']        = 'Cập nhật Phương thức Miễn phí Giao hàng';

// Entry
$_['entry_total']      = 'Giá trị:';
$_['entry_geo_zone']   = 'Vùng tính thuế: <span class="help">(<i>Chọn vùng muốn áp dụng thuế.</i>)</span>';
$_['entry_status']     = 'Trạng thái:';
$_['entry_sort_order'] = 'Thứ tự:';

// Help
$_['help_total']       = 'Giá trị tiền mua hàng khách phải đạt để được MIỄN phí cước Giao hàng, đơn vị tính Đồng.';



// Error
$_['error_permission'] = 'Cảnh báo: Bạn không có quyền chỉnh sửa Miễn phí Giao hàng!';
?>