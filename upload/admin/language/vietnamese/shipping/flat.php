<?php
// Heading
$_['heading_title']    = '<font color="green"><b>Cước Giao hàng Cố định</b></font>';

// Text
$_['text_shipping']    = 'Shipping';
$_['text_success']     = 'Thành Công: Bạn đã thay đổi flat rate shipping!';
$_['text_edit']        = 'Cập nhật Phương thức Cước Giao hàng Cố định';

// Entry
$_['entry_cost']       = 'Mức phí';
$_['entry_tax_class']  = 'Kiểu thuế';
$_['entry_geo_zone']   = 'Vùng tính thuế';
$_['entry_status']     = 'Trạng thái:';
$_['entry_sort_order'] = 'Thứ tự:';

// Error
$_['error_permission'] = 'Cảnh báo: Bạn không có quyền chỉnh sửa Cước Giao hàng Cố định!';
?>