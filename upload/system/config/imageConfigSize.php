<?php
$image_config_size_list = array (
	'config_image_location_width'		=>	268,
	'config_image_location_height'		=>	50,

	'config_image_cart_width'			=>	48,
	'config_image_cart_height'			=>	72,

	'config_image_wishlist_width'		=>	48,
	'config_image_wishlist_height'		=>	72,
	
	'config_image_compare_width'		=>	90,
	'config_image_compare_height'		=>	90,

	'config_image_related_width'		=>	320,
	'config_image_related_height'		=>	480,

	'config_image_additional_width' 	=>	48,
	'config_image_additional_height'	=>	72,

	'config_image_product_width'		=>	320,
	'config_image_product_height'		=>	480,

	'config_image_popup_width'			=>	640,
	'config_image_popup_height'			=>	960,

	'config_image_thumb_width'			=>	320,
	'config_image_thumb_height'			=>	480,

	'config_image_category_width'		=>	80,
	'config_image_category_height'		=>	80
);