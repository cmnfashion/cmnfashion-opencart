<?php
require_once ('sdk/autoload.php');
class ControllerFacebookFacebook extends Controller {
	public function index() {
		if (!isset($this->session->data['lastRouteToLoginFacebook'])) return;	
		$lastRouteToLoginFacebook = $this->session->data['lastRouteToLoginFacebook'];
		$redirectUrl = 'index.php';
		if ($lastRouteToLoginFacebook != '') {
			$redirectUrl = $this->url->link($lastRouteToLoginFacebook);
		}
		unset($this->session->data['lastRouteToLoginFacebook']);


		require_once(DIR_SYSTEM . "config/facebookLoginConfig.php");
		$fb = new Facebook\Facebook($facebookLoginConfig);

		$helper = $fb->getRedirectLoginHelper();

		try {
		  $accessToken = $helper->getAccessToken();
		} catch(Facebook\Exceptions\FacebookResponseException $e) {
			$this->session->data['loginFacebook'] = array ('status' => 'error');
			$this->response->redirect($redirectUrl);
		} catch(Facebook\Exceptions\FacebookSDKException $e) {
		  	$this->session->data['loginFacebook'] = array ('status' => 'error');
			$this->response->redirect($redirectUrl);
		}

		if (! isset($accessToken)) {
		  	$this->session->data['loginFacebook'] = array ('status' => 'error');
			$this->response->redirect($redirectUrl);
		}

		try {
		  $response = $fb->get('/me?fields=id,name,email,first_name,last_name', $accessToken->getValue());
		} catch(Facebook\Exceptions\FacebookResponseException $e) {
		  	$this->session->data['loginFacebook'] = array ('status' => 'error');
			$this->response->redirect($redirectUrl);
		} catch(Facebook\Exceptions\FacebookSDKException $e) {
		  	$this->session->data['loginFacebook'] = array ('status' => 'error');
			$this->response->redirect($redirectUrl);
		}

		$user_profile = $response->getGraphUser();

		$json = array ();

		if ($this->customer->isLogged()) {
			$this->session->data['loginFacebook'] = array ('status' => 'logged');
			$this->response->redirect($redirectUrl);
		}

		$_SERVER_CLEANED = $_SERVER;
		$_SERVER = $this->clean_decode($_SERVER);

		$_SERVER = $_SERVER_CLEANED;
	

		$this->load->model('account/customer');

		$email = $user_profile['email'];
		$firstname = $user_profile['first_name'];
		$lastname = $user_profile['last_name'];
		$password = $this->get_password($user_profile['id']);

		if($this->customer->login($email, $password)){
			$this->session->data['loginFacebook'] = array ('status' => 'logged');
			$this->response->redirect($redirectUrl);
		}

		$email_query = $this->db->query("SELECT `email` FROM " . DB_PREFIX . "customer WHERE LOWER(email) = '" . $this->db->escape(strtolower($email)) . "'");

		if($email_query->num_rows){
			
			$this->model_account_customer->editPassword($email, $password);
			
			if($this->customer->login($email, $password)){
				$this->session->data['loginFacebook'] = array ('status' => 'logged');
				$this->response->redirect($redirectUrl);
			}
			
		} else {
			$config_customer_approval = $this->config->get('config_customer_approval');
			$this->config->set('config_customer_approval',0);

			$add_data=array();
			$add_data['email'] = $email;
			$add_data['password'] = $password;
			$add_data['firstname'] = $firstname;
			$add_data['lastname'] = $lastname;
			$add_data['fax'] = '';
			$add_data['telephone'] = '';
			$add_data['company'] = '';
			//$add_data['address_1'] = '';
			//$add_data['address_2'] = '';
			$add_data['city'] = '';
			$add_data['postcode'] = '';
			$add_data['country_id'] = 0;
			$add_data['zone_id'] = 0;

			$this->model_account_customer->addCustomer($add_data);

			if($this->customer->login($email, $password)){
				unset($this->session->data['guest']);
				$this->session->data['loginFacebook'] = array ('status' => 'logged');
				$this->response->redirect($redirectUrl);
			}
		
		}

		// if fail!!!
		//$json['redirect'] = $this->url->link('account/account', '', 'SSL');
	}

	public function getLoginStatus() {
		if (isset($this->session->data['loginFacebook'])){
			$this->returnJson($this->session->data['loginFacebook']);
		}
		$this->returnJson(array ('status' => 'not-logged'));
	}

	public function getLoginUrl() {
		require_once(DIR_SYSTEM . "config/facebookLoginConfig.php");
		$fb = new Facebook\Facebook($facebookLoginConfig);

		$helper = $fb->getRedirectLoginHelper();
		$permissions = array('email');
		$currentUrl = $this->url->link('facebook/facebook');
		$loginUrl = $helper->getLoginUrl($currentUrl, $permissions);
		
		$route = $this->request->post['currentRoute'];

		// do not return to logout page!
		if ($route == 'account/logout') {
			$route = 'account/account';
		}

		$this->session->data['lastRouteToLoginFacebook'] = $route;
		$this->returnJson(
			array(
				'status'=>True,
				'loginUrl' => $loginUrl
			)
		);
	}

	private function returnJson($json) {
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	private function get_password($str) {
		$password = $this->config->get('fbconnect_pwdsecret') ? $this->config->get('fbconnect_pwdsecret') : 'fb';
		$password.=substr($this->config->get('fbconnect_apisecret'),0,3).substr($str,0,3).substr($this->config->get('fbconnect_apisecret'),-3).substr($str,-3);
		return strtolower($password);
	}

	private function clean_decode($data) {
    		if (is_array($data)) {
	  		foreach ($data as $key => $value) {
				unset($data[$key]);
				$data[$this->clean_decode($key)] = $this->clean_decode($value);
	  		}
		} else { 
	  		$data = htmlspecialchars_decode($data, ENT_COMPAT);
		}

		return $data;
	}	  
}
?>