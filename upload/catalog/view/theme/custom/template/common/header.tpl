<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->
<!--[if IE 9 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<!--<![endif]-->
<head>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<?php if ($description) { ?>
<meta name="description" content="<?php echo $description; ?>" />
<?php } ?>
<?php if ($keywords) { ?>
<meta name="keywords" content= "<?php echo $keywords; ?>" />
<?php } ?>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<?php if ($icon) { ?>
<link href="<?php echo $icon; ?>" rel="icon" />
<?php } ?>
<?php foreach ($links as $link) { ?>
<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
<?php } ?>
<script src="catalog/view/javascript/jquery/jquery-2.1.1.min.js" type="text/javascript"></script>
<link href="catalog/view/javascript/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen" />
<script src="catalog/view/javascript/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<link href="catalog/view/javascript/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="//fonts.googleapis.com/css?family=Open+Sans:400,400i,300,700" rel="stylesheet" type="text/css" />
<link href="catalog/view/theme/custom/stylesheet/stylesheet.css" rel="stylesheet">
<?php foreach ($styles as $style) { ?>
<link href="<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>" />
<?php } ?>
<script src="catalog/view/javascript/common.js" type="text/javascript"></script>
<?php foreach ($scripts as $script) { ?>
<script src="<?php echo $script; ?>" type="text/javascript"></script>
<?php } ?>
<?php echo $google_analytics; ?>
<script type="text/javascript">
  var cart_url = "<?php echo $shopping_cart ?>";
</script>

<script src="catalog/view/javascript/jquery/magnific/jquery.magnific-popup.min.js" type="text/javascript"></script>
<link href="catalog/view/javascript/jquery/magnific/magnific-popup.css" rel="stylesheet">

<script src="catalog/view/theme/custom/javascript/common.js" type="text/javascript"></script><!--customize-->

</head>
<body class="<?php echo $class; ?>">
<nav id="top">
  <div class="container">
    <div id="top-links" class="nav pull-right">
      <button type="button" id="categoryBtn" data-toggle="collapse" data-target=".navbar-ex1-collapse"><i class="fa fa-bars"></i></button>
      <div class="logo visible-xs">
        <a href="<?php echo $home; ?>">
          <span class="title"><?php echo $name; ?></span>
        </a>
      </div>
      <ul class="list-inline">
        <li><a href="javascript:void(0);"><i id="category-search-icon-smallscreen" class="fa fa-search visible-xs"></i></a></li>
        <li id="top_menu_wishlist_link"><a href="<?php echo $wishlist; ?>" id="wishlist-total" title="<?php echo $text_wishlist; ?>"><i class="fa fa-heart"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $text_wishlist; ?></span></a></li>
        <li id="top_menu_cart_icon">
          <?php echo $cart; ?>
        </li>
        <li id="top_menu_contact_link"><a href="<?php echo $contact; ?>" title="<?php echo $text_contact; ?>" ><i class="fa fa-phone"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $text_contact; ?></span></a></li>
        <?php if ($logged) { ?>
          <li id="top_menu_account_link" class="dropdown"><a href="<?php echo $account; ?>" title="<?php echo $text_account; ?>" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $text_account; ?></span> <span class="caret"></span></a>
          <ul id="top_menu_account" class="dropdown-menu dropdown-menu-right">
            <li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
            <li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
            <li><a href="<?php echo $transaction; ?>"><?php echo $text_transaction; ?></a></li>
            <li><a href="<?php echo $logout; ?>"><?php echo $text_logout; ?></a></li>
          </ul>
          </li>
        <?php } else { ?>
          <li id="top_menu_account_link"><a href="<?php echo $account; ?>" title="<?php echo $text_account; ?>" ><i class="fa fa-user"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $text_account; ?></span></a>
        <?php } ?>
      </ul>
    </div>
    <div id="top-search-container">
    </div>
  </div>
</nav>
<?php if ($categories) { ?>
<div id="categoryContainer" class="header">
  <div class="container">

    <div id="logo" class="nav pull-left hidden-xs">
        <a href="<?php echo $home; ?>">
          <img src="./image/catalog/logo/cmn_logo_black_100x100.png" class="img-responsive" />
          <span class="title"><?php echo $name; ?></span>
        </a>
    </div>

    <nav id="menu" class="smallScreen">
      <div id="menuCategory">
        <ul class="nav navbar-nav navbar-right">
          <?php foreach ($categories as $category) { ?>
          <?php if ($category['children']) { ?>
          <li class="dropdown"><a href="<?php echo $category['href']; ?>" class="dropdown-toggle" data-toggle="dropdown"><?php echo $category['name']; ?><b class="fa fa-chevron-down"></b></a>
            <div class="dropdown-menu">
              <div class="dropdown-inner">
                <?php foreach (array_chunk($category['children'], ceil(count($category['children']) / $category['column'])) as $children) { ?>
                <ul class="list-unstyled">
                  <?php foreach ($children as $child) { ?>
                  <li><a href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?></a></li>
                  <?php } ?>
                </ul>
                <?php } ?>
              </div>
              <a href="<?php echo $category['href']; ?>" class="see-all"><?php echo $text_all; ?> <?php echo $category['name']; ?></a> </div>
          </li>
          <?php } else { ?>
          <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
          <?php } ?>
          <?php } ?>
          <li id="search-container">
            <i id="category-search-icon" class="fa fa-search hidden-xs"></i>
            <header>
                  <?php echo $search; ?>
            </header>
          </li>
        </ul>
      </div>
    </nav>
  </div>
</div>
<?php } ?>

<?php if (!$logged) { ?>
<!-- login dialog -->
<div id="login-dialog" class="mfp-hide">
  <h1>Đăng nhập</h1>
  <div class="alert alert-danger" style="display:none">
    <i class="fa fa-exclamation-circle"></i> <span class="message"></span>
    <button type="button" class="close" data-dismiss="alert">&times;</button>
  </div>
  <form>
    <div class="form-group">
      <input type="text" name="email" value="" placeholder="Địa chỉ E-Mail:" class="form-control">
    </div>
    <div class="form-group">
      <input type="password" name="password" value="" placeholder="Mật khẩu:" class="form-control">
    </div>
    <div>
      <input type="submit" value="Đăng nhập" class="btn btn-primary btn-block btn-login">
    </div>
  </form>
  <div class="password-forgotten form-group"><a href="index.php?route=account/forgotten">Quên mật khẩu?</a></div>
  <div class="social-login form-group">
    <p>Đăng nhập bằng</p>
    <div><a class="btn btn-facebook-login btn-block" data-loading-text="Đang xử lý..." href="javascript:void(0)"><i class="fa fa-facebook"></i>Facebook</a></div>
  </div>
  <div class="login-footer">
    Chưa có tài khoản? <span class="register-account"><a href="index.php?route=account/register">Đăng ký ngay!</a></span>
  </div>
</div>
<!-- /login dialog -->
<?php } ?>