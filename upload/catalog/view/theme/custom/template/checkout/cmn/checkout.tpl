<?php echo $header; ?>

<link href="catalog/view/theme/custom/stylesheet/bootstrap-nav-wizard.css" rel="stylesheet">
<script src="catalog/view/theme/custom/javascript/localisation.js" type="text/javascript"></script>
<script src="catalog/view/theme/custom/javascript/checkout.js" type="text/javascript"></script>

<div class="container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <?php if ($error_warning) { ?>
  <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
    <button type="button" class="close" data-dismiss="alert">&times;</button>
  </div>
  <?php } ?>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <h1><?php echo $heading_title; ?></h1>

      <ul id="checkout-wizard-nav" class='nav nav-wizard'>
        <li class="active" data-step="checkout-option"><a>1. Đăng nhập</a></li>
        <li data-step="shipping-address"><a>2. Địa chỉ</a></li>
        <li data-step="checkout-confirm"><a>3. Xác nhận</a></li>
      </ul>

      <div class="panel-group" id="panel-checkout-steps">
        
        <div class="panel panel-default" id="panel-checkout-option">
          <div class="panel-heading">
            <h4 class="panel-title">Đăng nhập hoặc Đặt hàng không cần đăng ký</h4>
          </div>
          <div class="panel-collapse" id="collapse-checkout-option">
            <div class="panel-body">
                <div class="alert alert-danger" style="display:none">
                  <i class="fa fa-exclamation-circle"></i><span class="message"></span>
                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                </div>

                <p>Tùy Chọn Thanh Toán:</p>
                <div class="radio">
                    <label><input type="radio" name="account" id="radio-guest" checked="checked">Đặt hàng mà không cần đăng ký</label>
                </div>
                <div class="radio">
                    <label><input type="radio" name="account" id="radio-registered-user">Tôi đã có tài khoản</label>
                </div>
                
                <div id="form-login" class="form-login row">
                    <div class="col-sm-6">
                        <div class="form-group">
                          <label class="control-label" for="input-email">E-Mail:</label>
                          <input type="text" name="email" value="" placeholder="E-Mail:" id="input-email" class="form-control">
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="input-password">Mật khẩu:</label>
                            <input type="password" name="password" value="" placeholder="Mật khẩu:" id="input-password" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                  <input type="button" value="Tiếp tục" id="button-account-process" data-loading-text="Đang xử lý..." class="btn btn-primary">
                </div>
                <div class="social-login form-group">
                  <p>Đăng nhập bằng</p>
                  <div><a class="btn btn-facebook-login" data-loading-text="Đang xử lý..."><i class="fa fa-facebook"></i>Facebook</a></div>
                </div>
            </div>
          </div>
        </div>

        <div class="panel panel-default" id="panel-shipping-address">
          <div class="panel-heading">
            <h4 class="panel-title">Địa chỉ thanh toán và giao hàng</h4>
          </div>
          <div class="panel-collapse" id="collapse-shipping-address">
            <div class="panel-body">
              <div class="row">
                <div class="col-sm-6" id="shipping-address-column-account">
                  <fieldset id="account">
                    <legend>Thông tin tài khoản</legend>
                    <div class="form-group" style="display: none;">
                      <label class="control-label">Customer Group</label>
                      <div class="radio">
                        <label><input type="radio" name="customer_group_id" value="1" checked="checked">Default</label>
                      </div>
                    </div>
                    <div class="form-group required">
                      <label class="control-label" for="input-payment-firstname">Họ và Tên:</label>
                      <input type="text" name="fullname" value="" placeholder="Họ và Tên:" id="input-payment-fullname" class="form-control">
                    </div>
                    <div class="form-group required">
                      <label class="control-label" for="input-payment-email">E-Mail:</label>
                      <input type="text" name="email" value="" placeholder="E-Mail:" id="input-payment-email" class="form-control">
                    </div>
                    <div class="form-group required">
                      <label class="control-label" for="input-payment-telephone">Điện thoại:</label>
                      <input type="text" name="telephone" value="" placeholder="Điện thoại:" id="input-payment-telephone" class="form-control">
                    </div>
                  </fieldset>
                </div>
                <div class="col-sm-6">
                  <fieldset id="address">
                    <legend>Địa Chỉ</legend>

                    <div class="form-group" id="payment-address-options" style="display:none"> <!-- for registered user only -->
                      <div class="radio">
                        <label style="padding-left: 20px"><input type="radio" name="payment_address" value="existing" checked="checked">Tôi muốn dùng địa chỉ đã Đăng ký</label>
                      </div>
                      <div id="payment-existing">
                        <select name="address_id" class="form-control"></select>
                      </div>
                      <div class="radio">
                        <label style="padding-left: 20px"><input type="radio" name="payment_address" value="new">Tôi muốn dùng địa chỉ mới</label>
                      </div>
                    </div>

                    <div id="payment-address-form">
                      <div class="form-group required" id="form-group-payment-fullname-new" style="display:none"> <!-- for registered user only -->
                        <label class="control-label" for="input-payment-firstname">Họ và Tên:</label>
                        <input type="text" name="fullname" value="" placeholder="Họ và Tên:" id="input-payment-fullname-renew" class="form-control">
                      </div>
                      <div class="form-group required">
                        <label class="control-label" for="input-payment-address-1">Địa chỉ:</label>
                        <input type="text" name="address_1" value="" placeholder="Địa chỉ:" id="input-payment-address-1" class="form-control">
                      </div>
                      <div class="form-group required">
                        <label class="control-label" for="input-zone">Tỉnh / Thành phố</label>
                          <select name="zone_id" id="input-payment-zone" class="form-control">
                          </select>
                      </div>
                      <div class="form-group required">
                        <label class="control-label" for="input-payment-city">Quận / Huyện</label>
                        <select name="input-payment-city" id="input-payment-city" class="form-control">
                        <!-- initialized by Cmn.Localisation.District.initSelect -->
                        </select>
                      </div>
                      <div class="form-group required" id="form-group-payment-telephone" style="display:none"> <!-- for registered user only -->
                        <label class="control-label" for="input-payment-telephone">Điện thoại:</label>
                        <input type="text" name="telephone" value="" placeholder="Điện thoại:" id="input-payment-telephone-renew" class="form-control">
                      </div>
                    </div>
                  </fieldset>
                </div>
              </div>

              <div class="row">
                <div class="col-sm-6">
                  <fieldset>
                    <legend>Ghi chú</legend>
                    <div class="form-group required">
                      <textarea value="" placeholder="Thêm ghi chú cho đơn hàng" id="input-comment" class="form-control"></textarea>
                    </div>
                  </fieldset>
                </div>
              </div>

              <div class="buttons">
                <div class="pull-right">
                  <input type="button" value="Tiếp tục" id="button-shipping-address-process" data-loading-text="Đang xử lý..." class="btn btn-primary">
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="panel panel-default" id="panel-checkout-confirm">
          <div class="panel-heading">
            <h4 class="panel-title">Xác Nhận Đơn Hàng</h4>
          </div>
          <div class="panel-collapse" id="collapse-checkout-confirm">
            <div class="panel-body"></div>
          </div>
        </div>
      </div>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>

    <script type="text/javascript">
    Cmn.Checkout.data = <?php echo $json; ?>;
    </script>
</div>
<?php echo $footer; ?>