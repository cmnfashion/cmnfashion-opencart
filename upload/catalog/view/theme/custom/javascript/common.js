$(function(){
  var $header = $('.header');
  
  initLayout();
  initLoginDialog();
  initFacebookLogin();
  initHeaderSticky();
  initRedirectCheckout();

  $('#categoryBtn').click( function() {
    $('#menu').toggle();
    if($('#menu').is(":visible")) {
      $('#search').hide();
    }
  });

  $(window).resize(initLayout);

  $("#location_cart_link").click( function() {
      window.location.assign(cart_url)
  });

  $("#category-search-icon-smallscreen").click( function() {
    $('#search').toggle();
    if($('#search').is(":visible")) {
      $('#menu').hide();
    }
  });

  $("#menuCategory .dropdown").hover(function () {
    if($(window).width() > 750) {
      resetCategoryDropdown();
    }
  });

  //Fix focus top search box for mobile
  var isShowKeyboard = false;
  $('#search input').click(function(){
    isShowKeyboard = true;
  });
  $('#content').click(function(e) {
    isShowKeyboard = false;
    $('#search input').blur();
    $('#search').hide();
    $('#menu').hide();
  });

  function resetCategoryDropdown() {
      $("#menuCategory .dropdown").removeClass('open');
      $("#menuCategory .dropdown-toggle").attr('aria-expanded',false);
  }

  function initLayout() {
    if($(window).width() > 750) {
       $('#menu').show();
       $('#search').removeAttr('style').parent().appendTo("#search-container");
       resetCategoryDropdown();
    } else{
      $('#menu').hide();
      $('#search').parent().appendTo("#top-search-container");
      //Fix focus top search box for mobile 
      if(isShowKeyboard == true) {
        $('#search input').focus();
      };

    }
  }

  function initHeaderSticky() {
    $(window).scroll(function() {
      if($(this).scrollTop() > 100) {
        $header.addClass("sticky");
      } else {
        $header.removeClass("sticky");
      }
    });
  }

  function initRedirectCheckout() {
    // redirect to CMN checkout page instead of the default page of OpenCart
    $(document).on('click', 'a', function(e) {
      var link = e.currentTarget;
      if(!link.href.match(/checkout\/checkout/)) {
        return true;
      }
      location.href = link.href.replace('checkout/checkout', 'checkout/cmn/index');
      return false;
    });
  }

  function initLoginDialog() {
    var $loginDlg = $('#login-dialog');
    
    if ($loginDlg.length == 0) return;

    var $warning = $loginDlg.find('.alert');
    var $email = $loginDlg.find('[name="email"]');
    var $password = $loginDlg.find('[name="password"]');

    $loginDlg.submit(login);

    $('#top_menu_account_link').magnificPopup({
      items: { src: '#login-dialog', type: 'inline' }
    });

    function login(e) {
      e.preventDefault();
      $warning.hide();

      var data = {
        email: $.trim($email.val()),
        password: $password.val()
      };

      $.ajax({
          url: 'index.php?route=account/login_ajax',
          type: 'post',
          data: data
      })
      .done(function(json){
        if (json['logged']) {
          // reload page
          if (location.href.search('logout') > -1) {
            location.href = 'index.php?route=account/account';
          } else {
            location.reload();
          }
        } else if (json['warning']) {
          // display error
          $warning.find('.message').text(json['warning']);
          $warning.show();
        } else { // unexpected
          alert("ERROR:" + JSON.stringify(json));
        }
      });
    }
  }

  function initFacebookLogin() {
    $btnFbLogin = $('.btn-facebook-login');
    
    if ($btnFbLogin.length == 0) return;

    $btnFbLogin.click(function loginFacebook(e) {
      $btnFbLogin.button('loading');

      var redirectUrl = $(e.currentTarget).closest('.social-login').data('redirect'); // for login page
      var route = location.search.split('route=')[1] || 'common/home'; // for login dialog

      if (redirectUrl) {
        route = redirectUrl.match('route=(.*)')[1] || 'common/home';
      }
      
      $.ajax({
        url: 'index.php?route=facebook/facebook/getLoginUrl',
        type: 'post',
        data: {'currentRoute': route }
      })
      
      .done(function(result) {
        if (result.status === true) {
          // redirect to facebook for authentication
          location.href = result.loginUrl;
        } else { // error
          $btnFbLogin.button('reset');
          alert("Không thể đăng nhập bằng tài khoản Facebook. Xin vui lòng thử lại sau!");
        }
      });
    });
  }
});