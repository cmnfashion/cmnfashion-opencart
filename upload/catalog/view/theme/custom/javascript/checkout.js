/**
 * Checkout process customized for CMN Fashion
 * NOTE:
 * Current implementation will make shipping address and payment address be same
 */

Cmn = window.Cmn || {};

Cmn.Checkout = {
  init: function() {
    this._initWizardBar();
    this._initStepPanels();
    this._initCheckoutOption();
    this._initShippingAddress();
  },

  _initWizardBar: function() {
    Cmn.CheckoutWizardNav.init();
    Cmn.CheckoutWizardNav.on('change', function(stepId) {
      //console.log(stepId);
    });
  },

  _initStepPanels: function() {
    this.$checkoutSteps = $("#panel-checkout-steps");
    if(this.data.logged) {
      this._goStep('shipping-address');
    } else {
      this._goStep('checkout-option');
    }
  },

  _goStep: function(stepId) {
    Cmn.CheckoutWizardNav.active(stepId);

    this.$checkoutSteps.find('.panel').hide();
    $("#panel-" + stepId).show();
  },

  _initCheckoutOption: function() {
    var $formLogin = $('#form-login');
    var $nextButton = $('#button-account-process');

    toggleLoginForm();

    $('#radio-guest, #radio-registered-user').change(toggleLoginForm.bind(this));
    $('#button-account-process').click(handleCheckoutOption.bind(this));

    function toggleLoginForm() {
      $formLogin[$('#radio-guest').is(':checked') ? 'hide' : 'show']();
    }

    function handleCheckoutOption() {
      if ($('#radio-guest').is(':checked')) {
        this._goStep('shipping-address');
      } else {
        $nextButton.button('loading');
        
        this._login()
        .always(function(){
          $nextButton.button('reset');
        });
      }
    }
  },

  _initShippingAddress: function() {
    var self = this;

    var $nextButton = $('#button-shipping-address-process');

    var $newFullName = $('#form-group-payment-fullname-new');
    var $newTelephone = $('#form-group-payment-telephone');
    
    var $addressOptions = $('#payment-address-options');
    this.$addressOptions = $addressOptions;

    $addressOptions.find('input[name="payment_address"]').change(toggleAddressForm.bind(this));
    var $addressForm = $('#payment-address-form');
    var $addressDropdownList = $addressOptions.find('[name="address_id"]');
    this.$addressDropdownList = $addressDropdownList;

    Cmn.Localisation.District.initSelect('#input-payment-city');

    if (this.data.logged) {

      $("#shipping-address-column-account").hide();
      $addressForm.hide();
      
      populateAddresses(this.data.account_addresses);

      var hasAddress = $addressDropdownList.find('option').length != 0;
      if (hasAddress) {
        $addressOptions.show();
      } else {
        $addressOptions.find('input[name="payment_address"]')[1].checked = true;
        $addressOptions.hide();
        $addressForm.show();
        $newFullName.show();
        $newTelephone.show();
      }
      
      $nextButton.click(handleShippingForLoggedInUser);
    
    } else {
      $nextButton.click(handleShippingForGuest);
    }

    function handleShippingForGuest() {
      $nextButton.button('loading');
      
      self._savePaymentAddressForGuest().then(function(){
        return self._loadShippingMethods();
      }).then(function(){
        return self._saveShippingMethod();
      }).then(function(){
        return self._loadPaymentMethods();
      }).then(function(){
        return self._savePaymentMethod();
      }).then(function(){
        return self._goCheckoutConfirm();
      }).always(function(){
        $nextButton.button('reset');
      });
    }

    function handleShippingForLoggedInUser() {
      $nextButton.button('loading');

      self._savePaymentAddressForLoggedInUser().then(function(){
        return self._loadAddessesForLoggedInUser();
      }).then(function(addresses){
        return self._saveShippingAddressForLoggedInUser(addresses);
      }).then(function(){
        return self._loadShippingMethods();
      }).then(function(){
        return self._saveShippingMethod();
      }).then(function(){
        return self._loadPaymentMethods();
      }).then(function(){
        return self._savePaymentMethod();
      }).then(function(){
        return  self._goCheckoutConfirm();
      }).always(function(){
        $nextButton.button('reset');
      });
    }

    function populateAddresses(addresses) {
      if(!addresses) return;

      $.each(addresses, function(id, address){
        var addressForView = address.firstname + ' ' + address.lastname
          + ' - ' + address.address_1 + ', ' + address.city + ', ' + address.zone;

        $("<option/>")
          .text(addressForView)
          .val(address.address_id)
          .appendTo($addressDropdownList);
      });
    }

    function toggleAddressForm(e) {
      $newFullName.show();
      $newTelephone.show();
      $addressForm[e.target.value == 'existing' ? 'hide' : 'show']();
    }

  },

  _login: function() {
    var data = {
      email: $('#input-email').val(),
      password: $('#input-password').val()
    }
    var deferred = $.Deferred();
    var self = this;
    var $accountAlert = $('#panel-checkout-option .alert').hide();

    $.ajax({
        url: 'index.php?route=checkout/login/save',
        type: 'post',
        data: data
    })
    .done(function(json){
      if (json['redirect']) {
        self._redirect(json['redirect']);
      } else if (json['error']) {
        var warning = json['error'].warning;
        if (warning) {
          $accountAlert.find('.message').text(warning);
          $accountAlert.show();
        }
        deferred.rejectWith(self);
      } else { // success
        deferred.resolveWith(self);
      }
    });

    return deferred.promise();
  },

  _savePaymentAddressForGuest: function() {
    var name = this._getInputName('#input-payment-fullname');
    var $email = $("#input-payment-email");
    var $telephone = $("#input-payment-telephone");
    var $address = $("#input-payment-address-1");
    var $district = $("#input-payment-city"); // Quận/Huyện
    var $zone = $("#input-payment-zone"); // Tỉnh/Thành phố

    var data = {
      customer_group_id: 1, // default group
      firstname: name.firstname,
      lastname: name.lastname,
      email: $email.val(),
      telephone: $telephone.val(),
      fax: '',
      company: '',
      address_1: $address.val(),
      address_2: '',
      city: $district.val(),
      postcode: '--',
      country_id: 230, // vietnam
      zone_id: $zone.val(),
      shipping_address:1
    };

    var deferred = $.Deferred();
    var self = this;

    this._clearInputError($('#panel-shipping-address'));

    $.ajax({
        url: 'index.php?route=checkout/guest/save',
        type: 'post',
        data: data
    })
    .done(function(json){
      if (json['redirect']) {
        self._redirect(json['redirect']);
      } else if (json['error']) {
        showError(json['error']);
        deferred.rejectWith(self);
      } else { // success
        deferred.resolveWith(self);
      }
    });

    function showError(error) {
      if (error.firstname || error.lastname) {
        self._showInputError($('#input-payment-fullname'), 'Nhập đầy đủ họ và tên. ' + error.firstname + ' ' + (error.lastname || ''));
      }
      if (error.email) {
        self._showInputError($email, error.email);
      }
      if (error.telephone) {
        self._showInputError($telephone, error.telephone);
      }
      if (error.address_1) {
        self._showInputError($address, error.address_1);
      }
      if (error.city) {
        self._showInputError($district, error.city);
      }
      if (error.zone) {
        self._showInputError($zone, error.zone);
      }
    }

    return deferred.promise();
  },

  _savePaymentAddressForLoggedInUser: function() {
    var name = this._getInputName('#input-payment-fullname-renew');
    var $address = $('#input-payment-address-1');
    var $district = $('#input-payment-city');
    var $telephone = $('#input-payment-telephone-renew');
    var $zone = $('#input-payment-zone');

    var data = {
      address_id: this.$addressDropdownList.val(),
      payment_address: this.$addressOptions.find('input[name="payment_address"]:checked').val(),
      firstname: name.firstname,
      lastname: name.lastname,
      company: '',
      address_1: $address.val(),
      address_2: '',
      city: $district.val(),
      postcode: '--',
      country_id: 230, // vietnam
      zone_id: $zone.val(),
      telephone: $telephone.val()
    };

    var deferred = $.Deferred();
    var self = this;

    this._clearInputError($('#collapse-shipping-address'));

    $.ajax({
        url: 'index.php?route=checkout/payment_address/save',
        type: 'post',
        data: data
    })
    .done(function(json){
      if (json['redirect']) {
        self._redirect(json['redirect']);
      } else if (json['error']) {
        showError(json['error']);
        deferred.rejectWith(self);
      } else { // success
        deferred.resolveWith(self);
      }
    });

    function showError(error) {
      if (error.firstname || error.lastname) {
        self._showInputError($('#input-payment-fullname-renew'), 'Nhập đầy đủ họ và tên. ' + error.firstname + ' ' + (error.lastname || ''));
      }
      if (error.address_1) {
        self._showInputError($address, error.address_1);
      }
      if (error.city) {
        self._showInputError($district, error.city);
      }
      if (error.telephone) {
        self._showInputError($telephone, error.telephone);
      }
      if (error.telephone) {
        self._showInputError($zone, error.zone);
      }
      if (error.warning) {
        alert(error.warning);
      }
    }

    return deferred.promise();
  },

  _loadAddessesForLoggedInUser: function() {
    var deferred = $.Deferred();
    var self = this;

    $.ajax({
        url: 'index.php?route=checkout/cmn/index/addresses',
        type: 'post'
    })
    .done(function(json){
      deferred.resolveWith(self, [json]);
    });

    return deferred.promise();
  },

  _saveShippingAddressForLoggedInUser: function(addresses) {
    var deferred = $.Deferred();
    var self = this;

    var shippingAddress = this.$addressOptions.find('input[name="payment_address"]:checked').val();
    var addressId = this.$addressDropdownList.val();

    if (shippingAddress == 'new') {
      // get the new address which has been added in the previous step
      var addressIds = Object.keys(addresses);
      if (addressIds.length > 0) {
        addressId = addresses[addressIds[0]].address_id;
      }
    }

    var data = {
      shipping_address: 'existing',
      address_id: addressId,
      firstname: '',
      lastname: '',
      company: '',
      address_1: '',
      address_2: '',
      city: '',
      postcode: '',
      country_id: 230,
      zone_id: ''
    };

    $.ajax({
        url: 'index.php?route=checkout/shipping_address/save',
        type: 'post',
        dataType: 'json',
        data: data
    })
    .done(function(json){
      if (json['redirect']) {
        self._redirect(json['redirect']);
      } else if (json['error']) {
        //TODO: show error
        alert("ERROR:" + JSON.stringify(json));
        deferred.rejectWith(self);
      } else { // success
        deferred.resolveWith(self);
      }
    });

    return deferred.promise();
  },

  _loadShippingMethods: function() {
    var deferred = $.Deferred();
    var self = this;
    $.ajax({
      url: 'index.php?route=checkout/shipping_method',
      dataType: 'html'
    })
    .done(function(json){
      if (json['redirect']) {
        self._redirect(json['redirect']);
      } else if (json['error']) {
        //TODO: show error
        alert("ERROR:" + JSON.stringify(json));
        deferred.rejectWith(self);
      } else { // success
        deferred.resolveWith(self);
      }
    });

    return deferred.promise();
  },

  _saveShippingMethod: function() {
    var deferred = $.Deferred();
    var self = this;

    var data = {
      shipping_method: 'flat.flat',
      comment: $('#input-comment').val()
    };

    $.ajax({
        url: 'index.php?route=checkout/shipping_method/save',
        type: 'post',
        dataType: 'json',
        data: data
    })
    .done(function(json){
      if (json['redirect']) {
        self._redirect(json['redirect']);
      } else if (json['error']) {
        //TODO: show error
        alert("ERROR:" + JSON.stringify(json));
        deferred.rejectWith(self);
      } else { // success
        deferred.resolveWith(self);
      }
    });

    return deferred.promise();
  },

  _loadPaymentMethods: function() {
    var deferred = $.Deferred();
    var self = this;
    $.ajax({
      url: 'index.php?route=checkout/payment_method',
      dataType: 'html'
    })
    .done(function(json){
      if (json['redirect']) {
        self._redirect(json['redirect']);
      } else if (json['error']) {
        //TODO: show error
        alert("ERROR:" + JSON.stringify(json));
        deferred.rejectWith(self);
      } else { // success
        deferred.resolveWith(self);
      }
    });

    return deferred.promise();
  },

  _savePaymentMethod: function() {
    var deferred = $.Deferred();
    var data = {
      payment_method: 'cod',
      comment: $('#input-comment').val()
    };
    var self = this;
    $.ajax({
        url: 'index.php?route=checkout/payment_method/save',
        type: 'post',
        dataType: 'json',
        data: data
    })
    .done(function(json){
      if (json['redirect']) {
        self._redirect(json['redirect']);
      } else if (json['error']) {
        //TODO: show error
        alert("ERROR:" + JSON.stringify(json));
        deferred.rejectWith(self);
      } else { // success
        deferred.resolveWith(self);
      }
    });

    return deferred.promise();
  },

  _goCheckoutConfirm: function() {
    var self = this;

    $.ajax({
      url: 'index.php?route=checkout/confirm',
      dataType: 'html'
    })
    .done(function(html) {
      var $panelBody = $('#collapse-checkout-confirm .panel-body').html(html);
      $panelBody.parent().removeClass('collapse');
      self._goStep('checkout-confirm');
    })
    .fail(function(xhr, ajaxOptions, thrownError) {
      alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
    });
  },

  _redirect: function(url) {
    // should redirect to CMN checkout page instead of the default page of OpenCart
    url = url.replace('checkout/checkout', 'checkout/cmn/index');
    window.location.href = url;
  },

  _getInputName: function(fullnameControl) {
    var $fullname = $(fullnameControl);
    var fullnameValue = $.trim($fullname.val()).split(' ');
    var lastname = fullnameValue.pop();
    var firstname = fullnameValue.join(' ');

    return {
      lastname: lastname,
      firstname: firstname
    };
  },

  _showInputError: function($element, message) {
      $element.after('<div class="text-danger">' + message + '</div>');
      $element.parent().addClass('has-error');
  },

  _clearInputError: function($panel) {
    $panel.find('.form-group').removeClass('has-error');
    $panel.find('.text-danger').remove();
  }
};

Cmn.CheckoutWizardNav = {
  init: function() {
    this._event = $({});
    this.$el = $('#checkout-wizard-nav');
    this.$el.on('click', 'li', this._changeStep.bind(this));
  },
  active: function(stepId) {
    var $steps = this.$el.find('li');
    $steps.each(function(){
      var $step = $(this);
      $step.removeClass('active');
      if ($step.data('step') == stepId) {
        $step.addClass('active');
      }
    });
  },
  on: function(name, fn) {
    this._event.on(name, fn);
  },
  _changeStep: function(e) {
    var $step = $(e.target);

    if($step.hasClass('active')) return;

    this._event.trigger('change', $step);
  }
};

$(function(){
  Cmn.Checkout.init();


  $.ajax({
    url: 'index.php?route=account/account/country&country_id=230',
    dataType: 'json',
    beforeSend: function() {
      $('select[name=\'country_id\']').after(' <i class="fa fa-circle-o-notch fa-spin"></i>');
    },
    complete: function() {
      $('.fa-spin').remove();
    },
    success: function(json) {
      if (json['postcode_required'] == '1') {
        $('input[name=\'postcode\']').parent().parent().addClass('required');
      } else {
        $('input[name=\'postcode\']').parent().parent().removeClass('required');
      }

      html = '<option value="">--- Chọn ---</option>';

      if (json['zone'] && json['zone'] != '') {
        for (i = 0; i < json['zone'].length; i++) {
          html += '<option value="' + json['zone'][i]['zone_id'] + '"';

          if (json['zone'][i]['zone_id'] == '<?php echo $zone_id; ?>') {
            html += ' selected="selected"';
          }

          html += '>' + json['zone'][i]['name'] + '</option>';
        }
      } else {
        html += '<option value="0" selected="selected"><?php echo $text_none; ?></option>';
      }

      $('select[name=\'zone_id\']').html(html);
    },
    error: function(xhr, ajaxOptions, thrownError) {
      alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
    }
  });

  Cmn.Localisation.District.clearSelect('#input-payment-city');
  $("#input-payment-zone").on('change', function() {
    if(this.value === "3780") { //3780 is Ho Chi Minh
      Cmn.Localisation.District.initSelect('#input-payment-city', Cmn.Checkout.data.city);
    } else {
      Cmn.Localisation.District.clearSelect('#input-payment-city');
    }
  });
});