Cmn = window.Cmn || {};

Cmn.Localisation = {};

Cmn.Localisation.District = {
  availableDistricts: [
    'Quận 1',
    'Quận 10',
    'Quận 11',
    'Quận 12',
    'Quận 2',
    'Quận 3',
    'Quận 4',
    'Quận 5',
    'Quận 6',
    'Quận 7',
    'Quận 8',
    'Quận 9',
    'Quận Bình Tân',
    'Quận Bình Thạnh',
    'Quận Gò Vấp',
    'Quận Phú Nhuận',
    'Quận Tân Bình',
    'Quận Tân Phú',
    'Quận Thủ Đức',
    'Huyện Bình Chánh',
    'Huyện Củ Chi',
    'Huyện Hóc Môn',
    'Huyện Nhà Bè'
  ],

  initSelect: function(element, selectedItem) {
    var elementText = element.replace('#','');
    var $element = $(element);
    $element.replaceWith('<select name="city" id="'+elementText+'" class="form-control"></select>');
    $element = $(element);
    
    $element.append('<option value="">--- Chọn ---</option>');
    
    $.each(this.availableDistricts, function() {
      var district = this;
      var $option = $('<option/>')
        .text(district)
        .attr('value', district)
        .appendTo($element);

        if (district == selectedItem) {
          $option.attr('selected', 'selected');
        }
    });
  },
    clearSelect: function(element) {
        var elementText = element.replace('#','');
        var $element = $(element);
        $element.replaceWith('<input type="text" name="city" value="" placeholder="Quận / Huyện" id="' + elementText + '" class="form-control" />');
    }
};
