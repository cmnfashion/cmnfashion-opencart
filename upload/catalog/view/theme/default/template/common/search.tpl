<div id="search" class="hide">
	<table>
		<tr>
			<td>
				<input type="text" name="search" value="<?php echo $search; ?>" placeholder="<?php echo $text_search; ?>" class="form-control input-lg" />
			</td>
			<td class="button">
				<button type="button" class="btn btn-default btn-lg"><i class="fa fa-search"></i></button>
			</td>
		</tr>
	</table>
</div>