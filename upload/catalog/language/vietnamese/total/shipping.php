<?php
// Heading 
$_['heading_title']     = 'Ước tính chi phí Giao hàng &amp; Thuế';
// Text
$_['text_shipping']     = 'Nhập vào nơi đến để ước tính phí Giao hàng.';
$_['text_success']      = 'Thành công: Ước tính phí Giao hàng của bạn đã được áp dụng!';

// Entry
$_['entry_country']     = 'Quốc gia:';
$_['entry_zone']        = 'Tỉnh:';
$_['entry_postcode']    = 'Mã bưu điện:';

// Error
$_['error_postcode']    = 'Mã bưu điện từ 2 đến 10 kí tự!';
$_['error_country']     = 'Vui lòng chọn quốc gia!';
$_['error_zone']        = 'Vui lòng chọn Tỉnh!';
$_['error_shipping']    = 'Lỗi: Thiếu phương thức Giao hàng!';
$_['error_no_shipping'] = 'Lỗi: Không có phương thức Giao hàng nào. Vui lòng <a href="%s">Liên hệ chúng tôi</a> để được tư vấn!';
?>