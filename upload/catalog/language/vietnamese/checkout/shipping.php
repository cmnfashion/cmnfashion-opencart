<?php
// Heading
$_['heading_title']        = 'Ước lượng Cước Giao hàng &amp; Thuế';

// Text
$_['text_success']         = 'Chúc mừng! Phương thức Giao hàng cho đơn hàng đã xác định Thành công!';
$_['text_shipping']        = 'Xác định địa chỉ nhận hàng để tính cước Giao hàng.';
$_['text_shipping_method'] = 'Vui lòng chọn phương thức Giao hàng phù hợp cho Đơn hàng';

// Entry
$_['entry_country']        = 'Quốc gia';
$_['entry_zone']           = 'Tỉnh / Thành phố';
$_['entry_postcode']       = 'Mã Bưu điện';

// Error
$_['error_postcode']       = 'Mã Bưu điện phải từ 2 đến 10 ký tự!';
$_['error_country']        = 'Vui lòng xác định Quốc gia!';
$_['error_zone']           = 'Vui lòng xác định Quận / Huyện!';
$_['error_shipping']       = 'Chú ý! Hãy chọn phương thức Giao hàng!';
$_['error_no_shipping']    = 'Chú ý! Hiện không có phương thức Giao hàng thiết lập cho Sản phẩm này. Vui lòng <a href="%s"> liên hệ </a> để được trợ giúp thêm!';